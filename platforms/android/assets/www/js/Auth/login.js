angular.module( 'app.login', [
])
    .config(function($stateProvider) {
        $stateProvider.state('main.login', {
            url: '/login',
            views: {
                'main-login': {
                    templateUrl: 'templates/main-login.html',
                    controller: 'LoginController'
                }
            }
        });
    })
    .controller('LoginController', function($scope,  $http, $state, $ionicBackdrop, $ionicNavBarDelegate) {
        $ionicNavBarDelegate.title('Login');
        $scope.login = function(luser){
            $ionicBackdrop.retain();
            $http.post('http://104.236.200.201/auth/login', luser).
                success(function(data, status, headers, config) {
                    if(data != 'FAILED'){
                        window.localStorage.setItem('email', luser.email);
                        luser.email = '';
                        luser.password = '';
                        $ionicBackdrop.release();
                        $state.go("tab.cars");
                    }else{
                        alert("Something went wrong, please try again");
                        $ionicBackdrop.release();
                        luser.password = '';
                    }

                }).
                error(function(data, status, headers, config) {
                    $ionicBackdrop.release();
                    alert("error " + status);
                });
        };
    });

