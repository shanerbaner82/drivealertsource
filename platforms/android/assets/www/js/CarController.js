angular.module( 'starter')

    .controller('CarsController', function ($scope, $rootScope,  $ionicModal, $http, $cordovaContacts, $ionicLoading, $state) {
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        $scope.newCar = function () {
            $ionicModal.fromTemplateUrl('templates/modals/new_car_modal.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
        };

        $scope.makeCar = function (car) {
            var myEmail = window.localStorage.getItem('email');
            var myGcmid = window.localStorage.getItem('regid');
            car.email = myEmail;
            car.gcmid = myGcmid;
            $scope.postCar(car);
        };
        $scope.postCar = function (car) {
            console.log(JSON.stringify(car));
            $http.post("http://104.236.200.201/cars", JSON.stringify(car)).
                success(function (data, status, headers, config) {
                    $scope.closeModal();
                    $scope.refreshData();
                    //alert(data);
                }).
                error(function (data, status, headers, config) {
                    alert("error " + status);
                });
        };
        $scope.refreshData = function () {
            $ionicLoading.show({
                template: 'Getting Cars...'
            });
            var email = window.localStorage.getItem('email');
            $rootScope.email = email;
            $http.get('http://104.236.200.201/cars/' + email).
                success(function (data, status, headers, config) {
                    $rootScope.cars = data;
                    $scope.carsCount = data.length;
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                }).
                error(function (data, status, headers, config) {
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };
        $scope.editCar = function (car) {
            $rootScope.$emit('setTheCar', car);
            $state.go("editcar");
        };
        $scope.refreshData();
    });



