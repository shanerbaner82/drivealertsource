angular.module('starter').controller('ContactController', function($scope, $http, $ionicModal, $cordovaContacts, $ionicLoading, $state, $rootScope){

    $scope.refreshContactsData = function(){
        $ionicLoading.show({
            template: 'Getting Contacts...'
        });
        var email = window.localStorage.getItem('email');
        $http.get('http://104.236.200.201/contacts/'+email).
            success(function(data, status, headers, config) {
                $rootScope.savedContacts = data;
                $scope.savedContactsCount = data.length;
                $ionicLoading.hide();
                $scope.$broadcast('scroll.refreshComplete');
            }).
            error(function(data, status, headers, config) {
                $ionicLoading.hide();
                $scope.$broadcast('scroll.refreshComplete');
            });
    };
    $scope.closeModal = function () {
        $scope.modal.hide();
    };
    $scope.getContactList = function() {
        $ionicModal.fromTemplateUrl('templates/modals/contacts_modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
            $scope.modal.show();
        });
    };
    $scope.search = function(myContact){
        $cordovaContacts.find({filter:  myContact }).then(function(result) {
            $scope.contacts = result;
        }, function(error) {
            console.log("ERROR: " + error);
        });
    };
    $scope.addContact = function(contact){
        var newContact = {};
        newContact.name = contact.displayName;
        newContact.number = contact.phoneNumbers[0].value;
        newContact.user_email = window.localStorage.getItem('email');
        $http.post("http://104.236.200.201/contacts", JSON.stringify(newContact)).
            success(function(data, status, headers, config) {
                $scope.closeModal();
                $scope.refreshContactsData();
            }).
            error(function(data, status, headers, config) {
                alert("error " + status);
            });
    };
    $scope.editContact = function (contact) {
        $rootScope.$emit('setTheContact', contact);
        $state.go("edit_contact", {}, {reload: true});
    };

    $scope.refreshContactsData();
});