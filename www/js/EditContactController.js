angular.module('starter').controller('EditContactController', function($scope, $http, $state){


    $scope.editContact = function(editThisContact){
        $http.put('http://104.236.200.201/contacts/'+editThisContact.id, JSON.stringify(editThisContact)).
            success(function(d,s){
                $state.go("tab.contacts");
            }).
            error(function(d,s){
                alert(s);
            });
    };

    $scope.deleteContact = function(id){
        $http.delete('http://104.236.200.201/contacts/'+id).
            success(function(d,s){
                $state.go("tab.contacts");
            }).
            error(function(d,s){
                alert(s);
            });
    };
});
